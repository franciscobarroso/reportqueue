class WelcomeController < ApplicationController
  def index
    Resque.enqueue(ReportJob, note_params)
  end

  private
  def note_params
    params.permit(:text)
  end
end
